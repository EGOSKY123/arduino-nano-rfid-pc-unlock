#include <EEPROM.h>
#include <AESLib.h>

#include <Crypto.h>

#include <MFRC522.h>

#include <SPI.h>

#include <stdio.h>

#include <base64.hpp>

#include <string.h>

#define SAD 10
#define RST 9
using namespace std;
#define ledPinOpen 4
#define ledPinClosed 3

MFRC522 nfc(SAD, RST);

boolean Opening = false;
const int buzzer = 6;


void setup() {
  //Keyboard.begin();
  pinMode(ledPinOpen, OUTPUT);
  pinMode(ledPinClosed, OUTPUT);
  pinMode(buzzer, OUTPUT);
  SPI.begin();
  nfc.begin();
  Serial.begin(9600);
  byte version = nfc.getFirmwareVersion();

}

#define AUTHORIZED_COUNT 2 /*If you want more Authorized of cards set the count here, and then add the serials below*/

byte Authorized[AUTHORIZED_COUNT][4] = {
  {
    0x86,
    0x7B,
    0xD2,
    0x48,
  },
  {
    0xA5,
    0xA0,
    0x83,
    0x2C,
  } //example how to add more authorized cards
};

boolean isSame(byte * key, byte * serial);
boolean isAuthorized(byte * serial);

void loop() {
  byte status;
  byte data[MAX_LEN];
  byte serial[5];

  digitalWrite(ledPinOpen, Opening);
  digitalWrite(ledPinClosed, !Opening);

  status = nfc.requestTag(MF1_REQIDL, data);

  if (status == MI_OK) {
    status = nfc.antiCollision(data);
    memcpy(serial, data, 5);

    if (isAuthorized(serial)) {
      Opening = !Opening; // Interruttore ON-OFF
      delay(200);
      //send password hash with decryption key
      Serial.print("Card detected!");
      delay(500);
      //press enter key
      //send credentials to computer
      delay(200);
      //release keys
    } else {
      Opening = false;
    }

    nfc.haltTag();
    digitalWrite(ledPinOpen, Opening);
    digitalWrite(ledPinClosed, !Opening);
    delay(2000);
  }
  delay(500);
  handleCmd();
}

boolean isSame(byte * key, byte * serial) {
  for (int i = 0; i < 4; i++) {
    if (key[i] != serial[i]) {
      return false;
    }
  }
  return true;
}

boolean isAuthorized(byte * serial) {
  for (int i = 0; i < AUTHORIZED_COUNT; i++) {
    if (isSame(serial, Authorized[i]))
      return true;
  }
  return false;
}

//Function to process the setpassword command
void processPass(String cmd_string) {
  Serial.println("Command Recieved! Executing...");
  //Erase the "setpass:" substring from the main command string
  Serial.println(cmd_string);
  cmd_string.replace("setpass:", "");
  Serial.println("Checking validity...");
  if (cmd_string.length() != 1 or 0) {
    Serial.println("Generating Key...");
    uint8_t aes_key[] = "testkey12345";

    //Array to store the password
    char cred_strArray[cmd_string.length()+1];
    //Convert the password string to a char array
    cmd_string.toCharArray(cred_strArray, cmd_string.length()+1);
    //Start Encryption
    aes256_enc_single(aes_key, cred_strArray);

    Serial.print("Encrypted! Data is: ");
    printHex(cred_strArray);


    //Write the credentials to EEPROM
    Serial.println("Writing credentials to EEPROM...");
    writeStringToEEPROM(0,cred_strArray);
    Serial.println("Credentials stored successfully!");

    //Beep Buzzer
    buzzerdoubleBeep(1);
  } else {

    //Print Error Message if the password is missing
    Serial.println("Error: Missing password.");
  }
}

//Function to handle serial commands
void handleCmd() {
  while (Serial.available()> 0) {
  Serial.print("$ ");
  String command = Serial.readString();
  if (command.length() > 1) {
    Serial.println(command);
    //Process setpass command
    if (command.startsWith("setpass:") == 1) {
      processPass(command);
      //Process cleardata command
    } else if (command.startsWith("cleardata") == 1) {
      Serial.println("Clearing data stored in EEPROM...");
      for (int i = 0; i < EEPROM.length(); i++) {
        EEPROM.write(i, 0);
      }
      buzzerdoubleBeep(2);
      Serial.println("EEPROM has been reset!");
    } else if (command.startsWith("read") == 1) {
      String credentials = readStringFromEEPROM(0);
      Serial.println(credentials);
    } else {
      Serial.println("Invalid Command!");
    }
  }
}
}
// Function to beep the buzzer twice
void buzzerdoubleBeep(int number) {
  int i = 1;
  while (i <= number) {
    tone(buzzer, 2000);
    delay(250);
    noTone(buzzer);
    delay(100);
    tone(buzzer, 2000);
    delay(500);
    noTone(buzzer);
    delay(750);
    i++;
  }
}

//Function to write values to EEPROM
void writeStringToEEPROM(int addrOffset,
  const String & strToWrite) {
  byte len = strToWrite.length();
  EEPROM.write(addrOffset, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(addrOffset + 1 + i, strToWrite[i]);
  }
}
//Function to read from EEPROM
String readStringFromEEPROM(int addrOffset) {
  int newStrLen = EEPROM.read(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++) {
    data[i] = EEPROM.read(addrOffset + 1 + i);
  }
  data[newStrLen] = '\0'; // the character may appear in a weird way, you should read: 'only one backslash and 0'
  return String(data);
}

//Function to print a char array as HEX
void printHex (char array[]) {

  for (int i=0; i<sizeof(array); i++) {
   Serial.print(array[i], HEX);
   Serial.print(","); //separator
   }
   Serial.println("");
}