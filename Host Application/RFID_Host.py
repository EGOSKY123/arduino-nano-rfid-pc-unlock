import getopt
import getpass
import serial
from pynput.keyboard import Key, Controller
import sys

keyboard_control = Controller()


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "r", ["read"])
    except getopt.GetoptError:
        print('Args: --read')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-r", "--read"):
            read_credentials()


def read_credentials():
    connector = serial.Serial('COM4', 9600)
    data = connector.readline()
    data_decoded_raw = str(data[0:len(data)].decode("utf-8"))
    data_decoded = "".join(data_decoded_raw.split())

    if str(data_decoded).startswith("CRED:"):
        print("Recieved data! Decrypting...")
        data_decoded = data_decoded.replace("CRED:", "")
        credentials = data_decoded.split(",")

        # Syntax "CRED: decrypt_key, hash"

        decryption_key = credentials[0]
        password_hash = credentials[1]

    else:
        print(data_decoded)
        connector.close()



if __name__ == "__main__":
    main()
